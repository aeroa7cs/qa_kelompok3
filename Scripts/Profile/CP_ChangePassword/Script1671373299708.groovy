import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment(detail_case)

WebUI.callTestCase(findTestCase('Common/login'), [('email') : email, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Menu/btn_myAccount_menu'))

WebUI.verifyElementPresent(findTestObject('Dashboard/span_profile_dashboard'), 5)

WebUI.click(findTestObject('Dashboard/span_profile_dashboard'))

WebUI.verifyElementPresent(findTestObject('Profile/btn_ChangePassword_prof'), 5)

WebUI.click(findTestObject('Profile/btn_ChangePassword_prof'))

WebUI.verifyElementPresent(findTestObject('EditProfile/button_SaveChanges_editProf'), 5)

WebUI.setText(findTestObject('ChangePassword/input_OldPass_editpass'), oldpass)

WebUI.takeFullPageScreenshot()

WebUI.setText(findTestObject('ChangePassword/input_NewPass_editpass'), newpass)

WebUI.takeFullPageScreenshot()

WebUI.setText(findTestObject('ChangePassword/input_passConfirm_editpass'), passconfirm)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('EditProfile/button_SaveChanges_editprof'))

WebUI.verifyElementPresent(findTestObject('Profile/text_berhasil_prof'), 5)

WebUI.verifyElementText(findTestObject('Profile/div_successPassword_prof'), 'Kata sandi anda telah berubah')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Profile/button_OK'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Profile/btn_ChangePassword_prof'), 5)

WebUI.click(findTestObject('Profile/hover_logout_prof'))

WebUI.click(findTestObject('Profile/btn_logout_prof'))

WebUI.callTestCase(findTestCase('Common/login'), [('email') : email, ('password') : newpass], FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

