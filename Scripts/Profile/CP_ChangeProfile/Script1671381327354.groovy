import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment(case_name)

WebUI.comment(detail_case)

WebUI.callTestCase(findTestCase('Common/login'), [('email') : email, ('password') : password], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Menu/btn_myAccount_menu'))

WebUI.verifyElementPresent(findTestObject('Dashboard/span_profile_dashboard'), 5)

WebUI.click(findTestObject('Dashboard/span_profile_dashboard'))

WebUI.verifyElementPresent(findTestObject('Profile/btn_editProfile_prof'), 5)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Profile/btn_editProfile_prof'))

WebUI.verifyElementPresent(findTestObject('EditProfile/button_SaveChanges_editProf'), 5)

if (case_name.equals('cp_name') || case_name.equals('cp_profile')) {
    WebUI.setText(findTestObject('EditProfile/input_fullname_editprof'), newname)

    WebUI.takeFullPageScreenshot()
}

if (case_name.equals('cp_phone') || case_name.equals('cp_profile')) {
    WebUI.setText(findTestObject('EditProfile/input_phone_editprof'), newphone)

    WebUI.takeFullPageScreenshot()
}

if (case_name.equals('cp_tanggal') || case_name.equals('cp_profile')) {
    WebUI.setText(findTestObject('EditProfile/input_birthday_editprof'), newtanggal)

    WebUI.takeFullPageScreenshot()
}

WebUI.click(findTestObject('EditProfile/button_SaveChanges_editProf'))

WebUI.verifyElementPresent(findTestObject('Profile/text_berhasil_prof'), 5)

WebUI.verifyElementText(findTestObject('Profile/div_successUser_prof'), oldname + ' telah di edit')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Profile/button_OK'))

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Common/validityProfile'), [('newname') : newname, ('newno') : newphone, ('newtanggal') : newtanggal
        , ('case_name') : case_name], FailureHandling.STOP_ON_FAILURE)

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

