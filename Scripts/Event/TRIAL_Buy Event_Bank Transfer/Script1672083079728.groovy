import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.click(findTestObject('Menu/btn_login_menu'))

WebUI.setText(findTestObject('Events/input_email'), 'mtzfllcm@sharklasers.com')

WebUI.setText(findTestObject('Events/input_password'), '4Ec2byNPTH9T4Ex')

WebUI.click(findTestObject('Events/button_Login'))

WebUI.click(findTestObject('Events/btn_Events'))

WebUI.click(findTestObject('Events/div_OpenedEvent'))

WebUI.click(findTestObject('Events/btn_Beli_Tiket'))

WebUI.click(findTestObject('Events/btn_Lihat_Pembelian_Saya'))

WebUI.click(findTestObject('Events/button_Checkout'))

WebUI.click(findTestObject('Events/input_Total Pembayaran_payment_method'))

WebUI.click(findTestObject('Events/button_Confirm'))

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/method_payment'), 3)

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/payment_bank_transfer'), 3)

WebUI.click(findTestObject('Events/Payment Method/payment_bank_transfer'))

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/Bank Transfer/bca_va'), 3)

WebUI.click(findTestObject('Events/Payment Method/Bank Transfer/bca_va'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/btn_back_to_merchant'), 3)

WebUI.click(findTestObject('Events/Payment Method/btn_back_to_merchant'), FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

