import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeFullPageScreenshot('\\C:\\Katalon SS\\TBE07_1.jpg')

WebUI.click(findTestObject('Events/btn_Events'))

WebUI.verifyElementPresent(findTestObject('Events/List_Event'), 3)

WebUI.takeFullPageScreenshot('\\C:\\Katalon SS\\TBE07_2.jpg')

WebUI.click(findTestObject('Events/div_OpenedEvent'))

WebUI.verifyElementPresent(findTestObject('Events/btn_Beli_Tiket'), 5)

WebUI.takeFullPageScreenshot('\\C:\\Katalon SS\\TBE07_3.jpg')

WebUI.click(findTestObject('Events/btn_Beli_Tiket'))

WebUI.delay(3)

WebUI.takeFullPageScreenshot('\\C:\\Katalon SS\\TBE07_4.jpg')

WebUI.closeBrowser()

