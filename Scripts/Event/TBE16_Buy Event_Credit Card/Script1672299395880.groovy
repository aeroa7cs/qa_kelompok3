import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.click(findTestObject('Menu/btn_login_menu'))

WebUI.setText(findTestObject('Events/input_email'), 'tseventcc@sharklasers.com')

WebUI.setText(findTestObject('Events/input_password'), '1234zxcv')

WebUI.click(findTestObject('Events/button_Login'))

WebUI.click(findTestObject('Events/btn_Events'))

WebUI.verifyElementPresent(findTestObject('Events/List_Event'), 10)

WebUI.takeFullPageScreenshot()

WebUI.comment('TBE01')

WebUI.click(findTestObject('Events/div_OpenedEvent'))

WebUI.verifyElementPresent(findTestObject('Events/btn_Beli_Tiket'), 10)

WebUI.takeFullPageScreenshot()

WebUI.comment('TBE03')

WebUI.click(findTestObject('Events/btn_Beli_Tiket'))

WebUI.verifyElementPresent(findTestObject('Events/btn_Lihat_Pembelian_Saya'), 10)

WebUI.takeScreenshot()

WebUI.comment('TBE05')

WebUI.click(findTestObject('Events/btn_Lihat_Pembelian_Saya'))

WebUI.verifyElementPresent(findTestObject('Events/button_Checkout'), 10)

WebUI.takeFullPageScreenshot()

WebUI.comment('TBE07')

WebUI.click(findTestObject('Events/button_Checkout'))

WebUI.click(findTestObject('Events/input_Total Pembayaran_payment_method'))

WebUI.verifyElementClickable(findTestObject('Events/button_Confirm'))

WebUI.takeScreenshot()

WebUI.comment('TBE08')

WebUI.click(findTestObject('Events/button_Confirm'))

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/method_payment'), 10)

WebUI.takeScreenshot()

WebUI.comment('TBE11')

WebUI.verifyElementPresent(findTestObject('Object Repository/Events/Payment Method/payment_credit_card'), 10)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Object Repository/Events/Payment Method/payment_credit_card'))

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/Credit Card Field/btn_paynow_disable'), 10)

WebUI.setText(findTestObject('Events/Payment Method/Credit Card Field/field_card_number'), '4811111111111114')

WebUI.setText(findTestObject('Events/Payment Method/Credit Card Field/field_card_expiry'), '0125')

WebUI.setText(findTestObject('Events/Payment Method/Credit Card Field/field_card_cvv'), '123')

WebUI.verifyElementPresent(findTestObject('Events/Payment Method/Credit Card Field/btn_paynow_active'), 10)

WebUI.takeScreenshot()

WebUI.click(findTestObject('Events/Payment Method/Credit Card Field/btn_paynow_active'))

WebUI.delay(15)

WebUI.verifyElementPresent(findTestObject('Events/Menu/icon_profile'), 10)

WebUI.click(findTestObject('Events/Menu/icon_profile'))

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Events/Menu/my_account'), 10)

WebUI.click(findTestObject('Events/Menu/my_account'))

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Events/Invoice Menu/a_Invoice'), 10)

WebUI.click(findTestObject('Events/Invoice Menu/a_Invoice'))

WebUI.verifyElementPresent(findTestObject('Events/Invoice Menu/span_Completed'), 10)

WebUI.takeScreenshot()

WebUI.comment('TBE16')

WebUI.closeBrowser()

