import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('Selalu Update Nilai Pada Data Driven Sebelum Running Test')

WebUI.comment('Ini adalah test case ' + TestCaseId)

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeScreenshot()

WebUI.click(findTestObject('Menu/button_Buat_Akun'))

WebUI.waitForElementClickable(findTestObject('Register/button_Daftar'), 1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Name'), Nama)

WebUI.takeScreenshot()

if (!(TestCaseId.equals('TCRL20'))) {
    WebUI.setText(findTestObject('Register/input_Tanggal_Lahir'), TanggalLahir)

    WebUI.takeScreenshot()
}

WebUI.click(findTestObject('Register/label_konfirmasi_kata_sandi'))

WebUI.setText(findTestObject('Register/input_E-Mail'), Email)

WebUI.takeScreenshot()

if (TestCaseId.equals('TCRL43')) {
    WebUI.executeJavaScript('document.getElementById(\'whatsapp\').setAttribute(\'type\',\'text\')', [])
}

WebUI.setText(findTestObject('Register/input_Whatsapp'), Whatsapp)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Kata_Sandi'), KataSandi)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Konfirm_Kata_Sandi'), KonfirmasiKataSandi)

WebUI.takeScreenshot()

WebUI.check(findTestObject('Register/input_Check_Box'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

if (TestCaseId.equals('TCRL43')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_whatsapp_must_number'), 1)
} else if (TestCaseId.equals('TCRL15')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_min_age'), 1)
} else if (TestCaseId.equals('TCRL18')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_min_password'), 1)
} else if (TestCaseId.equals('TCRL19')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_password_not_match'), 1)
} else if (TestCaseId.equals('TCRL20')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_tanggal_lahir_required'), 1)
} else if (TestCaseId.equals('TCRL13')) {
    WebUI.verifyElementPresent(findTestObject('VerifikasiEmail/span_Verifikasi_Email'), 1)
} else if (TestCaseId.equals('TCRL17')) {
    WebUI.verifyElementPresent(findTestObject('Register/error_email_used'), 1)
} else if (TestCaseId.equals('TCRL33')) {
    WebUI.waitForElementClickable(findTestObject('Register/button_Kirim Ulang'), 61)

    WebUI.click(findTestObject('Register/button_Kirim Ulang'))

    WebUI.verifyElementPresent(findTestObject('Register/notif_success_resend_email'), 10)

    WebUI.takeScreenshot()
}

WebUI.closeBrowser()

