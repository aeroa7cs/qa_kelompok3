import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeScreenshot()

WebUI.click(findTestObject('Menu/button_Buat_Akun'))

WebUI.waitForElementPresent(findTestObject('Register/button_Daftar'), 1)

WebUI.takeScreenshot()

WebUI.comment('Ini adalah test case TCRL02')

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

//WebUI.click(findTestObject('Login/button_Login'))
WebUI.verifyElementAttributeValue(findTestObject('Register/input_Name'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.comment('Ini adalah test case TCRL04 dan TCRL09')

WebUI.setText(findTestObject('Register/input_Name'), 'User01')

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Tanggal_Lahir'), '17-Aug-1945')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Login/input_email_login'), 'name', 'email', 1)

WebUI.comment('Ini adalah test case TCRL05')

WebUI.setText(findTestObject('Register/input_E-Mail'), 'anantashop1990@gmail.com')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Register/input_Whatsapp'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.comment('Ini adalah test case TCRL21')

WebUI.setText(findTestObject('Register/input_Whatsapp'), '081234567890+-.')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Register/input_Whatsapp'), 'type', 'number', 1)

WebUI.comment('Ini adalah test case TCRL06')

WebUI.setText(findTestObject('Register/input_Whatsapp'), '081234567890')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Register/input_Kata_Sandi'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.comment('Ini adalah test case TCRL07')

WebUI.setText(findTestObject('Register/input_Kata_Sandi'), 'password123')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Register/input_Konfirm_Kata_Sandi'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.comment('Ini adalah test case TCRL11')

WebUI.setText(findTestObject('Register/input_Konfirm_Kata_Sandi'), 'password123')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Register/input_Check_Box'), 'validationMessage', 'Please check this box if you want to proceed.', 
    1)

//WebUI.setText(findTestObject('Login/input_email_login'), '')
//
//WebUI.setText(findTestObject('Login/input_password_login'), '')
//
//WebUI.click(findTestObject('Login/button_Login'))
WebUI.closeBrowser()

