import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeScreenshot()

WebUI.click(findTestObject('Menu/btn_login_menu'))

WebUI.waitForElementPresent(findTestObject('Login/button_Login'), 1)

WebUI.takeScreenshot()

WebUI.comment('Ini adalah test case TCRL22')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Login/input_email_login'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.comment('Ini adalah test case TCRL28')

WebUI.setText(findTestObject('Login/input_email_login'), 'cobata8880fanneat')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Login/input_email_login'), 'name', 'email', 1)

WebUI.comment('Ini adalah test case TCRL23')

WebUI.setText(findTestObject('Login/input_email_login'), 'cobata8880@fanneat')

WebUI.click(findTestObject('Login/button_Login'))

WebUI.takeScreenshot()

WebUI.verifyElementAttributeValue(findTestObject('Login/input_password_login'), 'validationMessage', 'Please fill out this field.', 
    1)

WebUI.closeBrowser()

