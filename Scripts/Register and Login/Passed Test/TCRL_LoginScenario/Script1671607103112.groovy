import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.comment('Ini adalah test case ' + CaseId)

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeScreenshot()

WebUI.click(findTestObject('Menu/btn_login_menu'))

WebUI.waitForElementClickable(findTestObject('Login/button_Login'), 1)

WebUI.takeScreenshot()

if (CaseId.equals('TCRL39')) {
    WebUI.executeJavaScript('document.getElementById(\'email\').removeAttribute(\'required\')', [])

    WebUI.executeJavaScript('document.getElementById(\'password\').removeAttribute(\'required\')', [])

    WebUI.click(findTestObject('Login/button_Login'))

    WebUI.verifyElementPresent(findTestObject('Login/error_email_required'), 1)

    WebUI.verifyElementPresent(findTestObject('Login/error_password_required'), 1)

    WebUI.takeScreenshot()

    WebUI.comment('Ini adalah test case TCRL39')
} else {
    WebUI.setText(findTestObject('Login/input_email_login'), Email)

    WebUI.takeScreenshot()

    WebUI.setText(findTestObject('Login/input_password_login'), Password)

    WebUI.takeScreenshot()

    WebUI.click(findTestObject('Login/button_Login'))

    WebUI.takeScreenshot()

    if (CaseId.equals('TCRL26') || CaseId.equals('TCRL27')) {
        WebUI.verifyElementPresent(findTestObject('Login/wrong_email_or_password'), 1)
    } else if (CaseId.equals('TCRL24')) {
        WebUI.verifyElementPresent(findTestObject('Login/pop_up_verifikasi'), 1)
    } else if (CaseId.equals('TCRL30')) {
        WebUI.verifyElementPresent(findTestObject('Menu/btn_myAccount_menu'), 1)
    }
}

WebUI.closeBrowser()

