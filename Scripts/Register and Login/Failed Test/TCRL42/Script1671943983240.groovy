import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.g_url)

WebUI.maximizeWindow()

WebUI.takeScreenshot()

WebUI.click(findTestObject('Menu/button_Buat_Akun'))

WebUI.waitForElementPresent(findTestObject('Register/button_Daftar'), 1)

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Name'), 'User01')

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Tanggal_Lahir'), '17-Aug-1945')

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/label_konfirmasi_kata_sandi'))

WebUI.comment('ganti dengan email belum pernah registrasi')

WebUI.setText(findTestObject('Register/input_E-Mail'), 'inssi@emailbukan.com')

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Whatsapp'), '081234567890')

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Kata_Sandi'), '12345678')

WebUI.takeScreenshot()

WebUI.setText(findTestObject('Register/input_Konfirm_Kata_Sandi'), '12345678')

WebUI.takeScreenshot()

WebUI.check(findTestObject('Register/input_Check_Box'))

WebUI.takeScreenshot()

WebUI.click(findTestObject('Register/button_Daftar'))

WebUI.takeScreenshot()

WebUI.verifyElementPresent(findTestObject('Register/button_Daftar'), 1)

WebUI.closeBrowser()

