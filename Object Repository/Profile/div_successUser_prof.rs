<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_successUser_prof</name>
   <tag></tag>
   <elementGuidId>5fa35a37-40ca-4fb4-a3d4-403248333870</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.swal-text</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = ' User Testing telah di edit ' or . = ' User Testing telah di edit ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>26be2328-6501-46a3-87df-c6abb0134663</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>swal-text</value>
      <webElementGuid>7b5d1f83-46d9-4310-8af5-c9bdc86275c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> User Testing telah di edit </value>
      <webElementGuid>bbaa9ea3-8686-4b83-9277-ea366edb2a0f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;light light-sidebar theme-white&quot;]/div[@class=&quot;swal-overlay swal-overlay--show-modal&quot;]/div[@class=&quot;swal-modal&quot;]/div[@class=&quot;swal-text&quot;]</value>
      <webElementGuid>b5f5e63b-cf2a-429c-9902-e479986d4e45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::div[1]</value>
      <webElementGuid>833ba0b0-416a-428a-a4db-d634d36d18db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coding.ID'])[2]/following::div[8]</value>
      <webElementGuid>f37aad5d-29c8-4f25-bdf6-cab53785b36a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OK'])[1]/preceding::div[1]</value>
      <webElementGuid>8d40d611-275e-4f2d-a56e-112e35d92688</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[11]</value>
      <webElementGuid>a39b13ee-32e3-4e2a-a71f-f3017a9f7d82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='User Testing telah di edit']/parent::*</value>
      <webElementGuid>1d1cab49-90c0-4812-b187-f84731b3871e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]</value>
      <webElementGuid>57acc8ae-6d87-4a66-bcd5-dc33934e036b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = ' User Testing telah di edit ' or . = ' User Testing telah di edit ')]</value>
      <webElementGuid>f53a80c1-e035-4555-bc95-f5cb32c01d16</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
