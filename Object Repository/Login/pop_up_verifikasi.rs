<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pop_up_verifikasi</name>
   <tag></tag>
   <elementGuidId>f83dd05e-e90c-4dba-b92a-023a23bbbfee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-content > div.modal-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>753d07fc-5145-4cb2-8b27-fd7f61e8a3f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>49ea140e-f76e-44a8-9307-4d3707b50eae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

                    
                        ×
                        

                        
                                                            Email harus terverifikasi
                                                    
                        
                                                                                    
                                                        Verifikasi
                        
                    



                </value>
      <webElementGuid>f6f7909a-d9bd-4b04-8ab9-ea8810c91845</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]</value>
      <webElementGuid>9afa5021-13b5-4ac0-9680-d8a41182b487</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div/div</value>
      <webElementGuid>a1305aa0-6cb7-4a08-a300-3ff880a59eed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat akun'])[1]/following::div[4]</value>
      <webElementGuid>6a638e8e-2846-4336-ba54-04b939d39c2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div</value>
      <webElementGuid>9e0c9635-cb4a-4412-a479-c6b430e63ee3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '

                    
                        ×
                        

                        
                                                            Email harus terverifikasi
                                                    
                        
                                                                                    
                                                        Verifikasi
                        
                    



                ' or . = '

                    
                        ×
                        

                        
                                                            Email harus terverifikasi
                                                    
                        
                                                                                    
                                                        Verifikasi
                        
                    



                ')]</value>
      <webElementGuid>a4cdf8d8-c70b-46e9-ba43-a1eacf0a28ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
