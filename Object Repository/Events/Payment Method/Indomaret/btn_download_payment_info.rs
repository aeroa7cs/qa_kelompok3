<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_download_payment_info</name>
   <tag></tag>
   <elementGuidId>8791e1a7-0ad9-466f-bfb8-da722d4a4b24</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'btn full tertiary' and (text() = 'Download payment info' or . = 'Download payment info')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>e0dde7d7-8ff5-4c1f-a668-7746d0cbb6a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full tertiary</value>
      <webElementGuid>c1c9d44d-f589-4b21-8fb9-39086393fb3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Download payment info</value>
      <webElementGuid>f764d63e-f847-4eb2-bdc7-efb3682ea252</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Events/Payment Method/method_payment</value>
      <webElementGuid>0676f0d8-38d5-4838-aac3-a0019ed43ff4</webElementGuid>
   </webElementProperties>
</WebElementEntity>
