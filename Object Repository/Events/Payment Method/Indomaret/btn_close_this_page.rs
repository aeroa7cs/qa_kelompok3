<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_close_this_page</name>
   <tag></tag>
   <elementGuidId>60d2fec3-9d4e-4c6d-9c1a-be412538cd34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'btn full primary' and (text() = 'Close this page' or . = 'Close this page')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>b597aec3-6f4b-4719-9de4-a761a4c25292</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>9b534e7a-bd8e-4bbf-8335-bfdef98643b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Close this page</value>
      <webElementGuid>54420608-2377-421f-b5fb-e6df87648343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Events/Payment Method/method_payment</value>
      <webElementGuid>fd3310de-2721-4e96-a46d-ef9aa201b240</webElementGuid>
   </webElementProperties>
</WebElementEntity>
