<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>method_payment</name>
   <tag></tag>
   <elementGuidId>07032a86-f826-4ca1-b604-83f36ebc1c73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//iframe[@id = 'snap-midtrans']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>97880c96-2dd0-44a9-927b-ce07aaaa0da0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>7eb49477-abfe-4868-9626-210b94a704cc</webElementGuid>
   </webElementProperties>
</WebElementEntity>
