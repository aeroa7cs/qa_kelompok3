<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_back_to_merchant</name>
   <tag></tag>
   <elementGuidId>2021e4cf-e3ea-4dcb-81be-c550a8ac86ff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'btn full primary' and (text() = 'Back to merchant' or . = 'Back to merchant')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8f155cf2-534e-4466-a387-7fb5788f3630</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>1186f6cb-4659-4937-a0f3-768dbff8cf60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Back to merchant</value>
      <webElementGuid>07310368-30a2-42a7-abdc-6511ac1ec2a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Events/Payment Method/method_payment</value>
      <webElementGuid>b86773fd-852d-443b-bbd3-fc0d75d371e7</webElementGuid>
   </webElementProperties>
</WebElementEntity>
