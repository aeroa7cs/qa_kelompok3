<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_paynow_active</name>
   <tag></tag>
   <elementGuidId>8d4930c4-696e-41d2-83bb-d8073fb96466</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'btn full primary']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>933c0036-4914-47cc-9db9-685ccecff9c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full primary</value>
      <webElementGuid>f0adc543-6743-447e-8a81-6f19cf212aef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Events/Payment Method/method_payment</value>
      <webElementGuid>6f44c433-0426-46f6-bf59-d3d728409ae3</webElementGuid>
   </webElementProperties>
</WebElementEntity>
