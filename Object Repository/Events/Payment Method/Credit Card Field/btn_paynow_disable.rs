<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_paynow_disable</name>
   <tag></tag>
   <elementGuidId>7a9ddf9e-b72d-4b26-b6c8-127270ce3a45</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class = 'btn full disabled inactive' and @ref_element = 'Object Repository/Events/Payment Method/method_payment']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>0d10bcb5-7a04-44ec-8c8b-fda5104dfe4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn full disabled inactive</value>
      <webElementGuid>25a0aaff-9cc6-4aab-9f90-9a34d4672d10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Events/Payment Method/method_payment</value>
      <webElementGuid>7fb61c06-5d6e-4f4e-a5fe-80290b97d299</webElementGuid>
   </webElementProperties>
</WebElementEntity>
