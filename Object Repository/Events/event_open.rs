<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>event_open</name>
   <tag></tag>
   <elementGuidId>b03b6af4-f027-495a-8937-9aa4516b8606</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='blockListEvent']/a/div/div[2]/h5)[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockBody&quot;]/h5[@class=&quot;textOpen&quot;][count(. | //h5[@class = 'textOpen']) = count(//h5[@class = 'textOpen'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h5</value>
      <webElementGuid>a7204e0c-6caf-42dc-930d-1c0e6ded60c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>textOpen</value>
      <webElementGuid>b3ac842d-cde5-407d-8f8a-7a5c1a678642</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                </value>
      <webElementGuid>f2762e55-0340-4a2f-99df-a6b268b91cdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loopevent&quot;)/li[@id=&quot;blockListEvent&quot;]/a[1]/div[@class=&quot;cardOuter&quot;]/div[@class=&quot;blockBody&quot;]/h5[@class=&quot;textOpen&quot;]</value>
      <webElementGuid>1c643845-59c4-4670-8f89-5ab82bc935f2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='blockListEvent']/a/div/div[2]/h5)[2]</value>
      <webElementGuid>fae37fdd-af7e-4902-9a9f-2a0c3bf58cd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='With Ziyad Syauqi Fawwazi'])[2]/following::h5[1]</value>
      <webElementGuid>fbe6a1f2-b2d4-452d-9f8f-4c2ce5939fff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 4: Workshop'])[1]/following::h5[2]</value>
      <webElementGuid>9dce28ea-45ac-4139-81f1-72e670e2cffe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mini Class'])[2]/preceding::h5[1]</value>
      <webElementGuid>c4e3d060-c1ff-43a7-a86b-7900eef7544b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a/div/div[2]/h5</value>
      <webElementGuid>cdbfc208-5320-4f44-80da-ba17634afb2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h5[(text() = 'OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                ' or . = 'OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                ')]</value>
      <webElementGuid>c477caa9-526b-4bd8-876d-eabd7edb7824</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
