<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Events</name>
   <tag></tag>
   <elementGuidId>839e315c-b471-4585-8a2e-5f5faa2a2a6f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='containerEvent']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#containerEvent</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;containerEvent&quot;)[count(. | //div[@id = 'containerEvent' and @class = 'container']) = count(//div[@id = 'containerEvent' and @class = 'container'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e91a3e7f-ef5a-480c-83e6-2b11a0a8bbb1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>containerEvent</value>
      <webElementGuid>e59a6016-63a2-43ae-afe2-f532815b4142</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container</value>
      <webElementGuid>e85d6caf-698e-469f-8c0f-43dd9bee2559</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>


        
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    





    </value>
      <webElementGuid>92521950-570d-4550-b3df-b8da76749984</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;containerEvent&quot;)</value>
      <webElementGuid>a0eceb83-735a-4581-9797-7f6861cc5e7b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='containerEvent']</value>
      <webElementGuid>b9e8e504-53af-4608-a3a5-11088a51e3fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CODING.ID Event'])[1]/following::div[2]</value>
      <webElementGuid>a42330c8-2afa-4c65-b18d-6c065c0c3534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div[4]/div[2]</value>
      <webElementGuid>25a891ca-cd6b-423d-8c43-a6f1a7dd9a51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'containerEvent' and (text() = '


        
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    





    ' or . = '


        
    
        
            Events
        
        

            
        
    


    
                    
                
                    
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 3: Predict using Machine Learning
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        18 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 3: Predict using Machine Learning
                                                
                                            
                                            
                                                18 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 4: Workshop
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            OPEN until
                                                        25 Nov 2023 11:30 WIB
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 4: Workshop
                                                
                                            
                                            
                                                25 Nov 2023 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 2: Data Wrangling with Python
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 2: Data Wrangling with Python
                                                
                                            
                                            
                                                11 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Day 1: Introduction to Python for Data Scientist
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Day 1: Introduction to Python for Data Scientist
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    Rp
                                                                85.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                    
                                                
                                                
                                                    With Ziyad Syauqi Fawwazi
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Booster Class: Be Data Scientist and Machine Learning Engineer in 4 Days
                                                
                                            
                                            
                                                04 Nov 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            2.000.000

                                                                                                                    Rp
                                                                200.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Automation Testing 101 (Demo Using Katalon)
                                                    
                                                
                                                
                                                    With Impola Tonatua Surya
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Automation Testing 101 (Demo Using Katalon)
                                                
                                            
                                            
                                                27 Oct 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            350.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    UI/UX Research &amp; Design (Hands On With Figma)
                                                    
                                                
                                                
                                                    With Sindy Larasati
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                UI/UX Research &amp; Design (Hands On With Figma)
                                                
                                            
                                            
                                                01 Oct 2022 |
                                                09:00 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    Shifting Career To Data Science
                                                    
                                                
                                                
                                                    With Eric Julianto
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                Shifting Career To Data Science
                                                
                                            
                                            
                                                16 Sep 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            500.000

                                                                                                                    FREE
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                                    
                                
                                    
                                        
                                            
                                                
                                                                                                    
                                                    
                                                    
                                                                                            

                                            
                                                
                                                    How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                    
                                                
                                                
                                                    With Denny Yusuf
                                                    
                                                
                                            

                                        
                                        
                                                                                            CLOSE
                                                
                                                                                        Mini Class
                                            
                                            
                                                How to Create an Effective &quot;Product Requirement Document&quot; (PRD)
                                                
                                            
                                            
                                                05 Aug 2022 |
                                                19:30 WIB Via
                                                Zoom
                                                
                                            
                                                                                                                                                Rp.
                                                            300.000

                                                                                                                    Rp
                                                                78.000
                                                                                                            
                                                                                                                                    
                                    
                                
                            
                                            
                
            
            

            
            
                Muat Lebih Banyak
                
            
        

        
            
                Loading...
            
        
    





    ')]</value>
      <webElementGuid>dfcd6cf9-4ecc-44d2-8110-e084a8836698</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
